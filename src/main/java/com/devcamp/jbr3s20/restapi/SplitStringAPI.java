package com.devcamp.jbr3s20.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> splitString() {
        ArrayList<String> arrayList = new ArrayList<>();
        String s1 = "red orange yellow green blue indigo violet";

        String[] words = s1.split("\\s");// tach chuoi dua tren khoang trang
        // su dung vong lap foreach de in cac element cua mang chuoi thu duoc
        for (String w : words) {
          
            arrayList.add(w);
        }
        return  arrayList;
    }
}